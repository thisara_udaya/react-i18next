import axios from 'axios';

async function getFiles(){

    let fileList = {};

    await axios.get('http://localhost:8080/files')
    .then(function(response){
        fileList = response.data;
    }).catch(function(rejected){
        console.log('error : ' + rejected);
    });

    return fileList;
}

async function getFile(moduleName, fileName){

    let file = {};

    await axios.get('http://localhost:8080/file/'+moduleName+'/'+fileName)
            .then(function(response){
                file = response.data;
            }).catch(function(rejected){
                console.log('error : ' + rejected);
            });

    return file;
}

async function updateFile(data){

    let res = '';

    await axios.post('http://localhost:8080/file', data
        ).then(function(response){
            res = response.status;
        }).catch(function(errorMessage){
            console.log(errorMessage);
        });

    return res;
}

export {
    getFile,
    updateFile,
    getFiles
}
