import React, { Component } from 'react';
import { FixedSizeList as List } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

import RowTemplate from '../commons/rowTemplates/listRowTemplate';
import FormHeader from '../commons/formHeaders/listFormHeader';
import { VERTICAL, HORIZONTAL, ALIGN_AUTO } from '../commons/constants/formHeaderConstants';

class AutoSizerList extends Component{

    listRef = React.createRef();

    state = {
        layout : VERTICAL,
        scrollToRowNumber : 1,
        align : ALIGN_AUTO
    }

    onChangeLayout = (e) => {
        if(e){
            this.setState({
                layout : e 
            });
        }
    }

    onScrollTo = (e) => {
        this.listRef.current.scrollToItem(this.state.scrollToRowNumber, this.state.align);
    }

    onUpdateScrollTo = (e) => {
        if(e){
            this.setState({
                scrollToRowNumber : e.target.value
            })
        }
    }

    onUpdateAlign = (e) => {
        if(e){
            this.setState({
                align : e.target.value
            })
        }
    }

    render(){
        return(
            <>
                <FormHeader formHeader={'list:title'}  VERTICAL={VERTICAL} HORIZONTAL={HORIZONTAL} layout={this.state.layout} onChangeLayout={this.onChangeLayout} onUpdateScrollTo={this.onUpdateScrollTo} onScrollTo={this.onScrollTo} onUpdateAlign={this.onUpdateAlign}/>
                <AutoSizer>
                    {({ height, width }) => (
                    <List
                        height={height-145}
                        itemCount={100}
                        itemSize={90}
                        width={width}
                        layout={this.state.layout}
                        useIsScrolling {...this.props}
                        ref={this.listRef}
                        className='listArea'
                    >
                        {RowTemplate}
                    </List>
                    )}
            </AutoSizer>
          </>
        )
    }
}

export default AutoSizerList;