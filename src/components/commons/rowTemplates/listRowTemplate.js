import React from 'react';
import { Row, Col} from 'react-bootstrap';

import '../../../styles/styles.css';
import Localizer from '../intl/localizer';

const RowTemplate = (props) => {

    const { isScrolling, style, index } = props;

    if(!isScrolling){
    return(
        <Row className={index % 2 ? 'ListItemOdd' : 'ListItemEven'} style={style}>
            <Col>
                <Localizer message='list:gridRow.columnText'/> 1 : <Localizer message='list:gridRow.rowText'/> {index}
            </Col>
            <Col>
                <Localizer message='list:gridRow.columnText'/> 2 : <Localizer message='list:gridRow.rowText'/> {index}
            </Col>
            <Col>
                <Localizer message='list:gridRow.columnText'/> 3 : <Localizer message='list:gridRow.rowText'/> {index}
            </Col>
        </Row>
    )
    }else{
        return(
            <Row className={index % 2 ? 'ListItemOdd' : 'ListItemEven'} style={style}>
                <Col>
                    <Localizer message='list:gridRow.loadingText'/>
                </Col>
            </Row>
        )   
    }
}

export default RowTemplate;