import i18n from 'i18next';

import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';

import homeEN from '../../../assets/lang/home/en.json';
import homeES from '../../../assets/lang/home/es.json';
import commonsEN from '../../../assets/lang/commons/en.json';
import commonsES from '../../../assets/lang/commons/es.json';
import listEN from '../../../assets/lang/list/en.json';
import listES from '../../../assets/lang/list/es.json';

const resources = {
  en: {
    home: homeEN, 
    commons: commonsEN,
    list: listEN
  },
  es: {
    home: homeES,
    commons: commonsES,
    list: listES
  }
};

const detectionOptions = {
  order: ['cookie'],
  lookupCookie: 'i18next'
}

i18n
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    resources,
    fallbackLng: 'en',
    debug: true,
    //detection: detectionOptions, // optional - use additional options
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
  });

export default i18n;
