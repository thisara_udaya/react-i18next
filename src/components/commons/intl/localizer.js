import React from 'react';
import { useTranslation } from 'react-i18next';

const Localizer = (props) => {
    
    const {t} = useTranslation();

    return(
        <>
            {t(props.message)}
        </>
    )
}

export default Localizer;