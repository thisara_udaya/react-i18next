const VERTICAL = 'vertical';
const HORIZONTAL = 'horizontal';

const ALIGN_START = "start";
const ALIGN_END = "end";
const ALIGN_CENTER = "center";
const ALIGN_AUTO = "auto";
const ALIGN_SMART = "smart";

export {
    VERTICAL,
    HORIZONTAL,
    ALIGN_AUTO,
    ALIGN_CENTER,
    ALIGN_END,
    ALIGN_SMART,
    ALIGN_START
}