const EN = 'en';
const ES = 'es';

const ENGLISH = 'English';
const SPANISH = 'Española'

export {
    EN,
    ES,
    ENGLISH,
    SPANISH
}