import React from 'react';
import { ButtonGroup, DropdownButton, Dropdown, Button, Form, Container, Row, Col } from 'react-bootstrap';

import { ALIGN_AUTO, ALIGN_START, ALIGN_SMART, ALIGN_END, ALIGN_CENTER } from '../constants/formHeaderConstants';
import Localizer from '../intl/localizer';

const FormHeader = (props) => {

    return (
        <Container className='formHeader'>
            <Row>
                <Col aria-colspan={4}>
                    <h2 className='formTitle'><Localizer message={props.formHeader}/></h2>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form.Control type="number" onChange={props.onUpdateScrollTo}/>
                </Col>
                <Col>
                    <ButtonGroup onClick={props.onUpdateAlign}>
                        <Button variant="secondary" value={ALIGN_AUTO}><Localizer message='list:formHeader.autoOption'/></Button>
                        <Button variant="secondary" value={ALIGN_SMART}><Localizer message='list:formHeader.smartOption'/></Button>
                        <Button variant="secondary" value={ALIGN_CENTER}><Localizer message='list:formHeader.centerOption'/></Button>
                        <Button variant="secondary" value={ALIGN_END}><Localizer message='list:formHeader.endOption'/></Button>
                        <Button variant="secondary" value={ALIGN_START}><Localizer message='list:formHeader.startOption'/></Button>
                    </ButtonGroup>
                </Col>
                <Col>
                    <div className='formTitleSearchButton'>
                        <Button variant="success" onClick={props.onScrollTo}><Localizer message='list:formHeader.search'/></Button>
                        <DropdownButton className='formTitleLayoutButton' variant="success" id="dropdown-basic-button" title={props.layout.toUpperCase()} onSelect={props.onChangeLayout}>
                            <Dropdown.Item eventKey={props.VERTICAL}><Localizer message='list:formHeader.verticalLayout'/></Dropdown.Item>
                            <Dropdown.Item eventKey={props.HORIZONTAL}><Localizer message='list:formHeader.horizontalLayout'/></Dropdown.Item>
                        </DropdownButton>
                    </div>    
                </Col>
                <Col area-aria-colspan={7}>
                    <></>
                </Col>
            </Row>
        </Container>);
}

export default FormHeader;