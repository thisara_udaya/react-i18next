import React, { Component } from 'react';
import { DropdownButton, Dropdown } from 'react-bootstrap';

import Localizer from '../intl/localizer';
import i18n from '../intl/i18n';
import {
    EN, ES, ENGLISH, SPANISH
}from '../constants/locals';

class LanguageButton extends Component {

    state = {
        selectedFlag : ''
    }

    componentDidMount(){
        this.setState({
            selectedFlag : this.getSelectedFlag()
        })
    }

    changeLanguage = (e) => {
        i18n.changeLanguage(e);
        this.setState({
            selectedFlag : this.getSelectedFlag()
        })
    }

    getSelectedFlag = () => {
        return <Localizer message='commons:header.language'/>;
    }

    render(){
        return (
            <div className="fd" className='langStyle'>
                <DropdownButton variant="success" id="dropdown-basic-button" title={this.state.selectedFlag} onSelect={this.changeLanguage}>
                    <Dropdown.Item eventKey={EN}>{ENGLISH}</Dropdown.Item>
                    <Dropdown.Item eventKey={ES}>{SPANISH}</Dropdown.Item>
                </DropdownButton>
            </div>
        );
    }
}

export default LanguageButton;