import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import Localizer from '../components/commons/intl/localizer';

class HomePage extends Component {
    render(){
        return(
            <Container className='formHeader'>
                <Row>
                    <Col>
                        <h2 className='formTitle'>
                            <Localizer message='home:title' />
                        </h2>
                        <p>
                            <Localizer message='home:description.paragraph1'/>
                        </p>
                        <p>
                            <Localizer message='home:description.paragraph2'/>
                        </p>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default HomePage;