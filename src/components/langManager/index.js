import React, { Component } from 'react';
import { FixedSizeList as List } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

import FormHeader from '../commons/formHeaders/listFormHeader';
import { VERTICAL, HORIZONTAL } from '../commons/constants/formHeaderConstants';
import LangRowTemplate from './langRowTemplate';
import {
    getFiles
}from '../../services'

class LangManager extends Component{

    state = {
        fileList : {},
        layout : VERTICAL,
        isEdit : false
    }

    async componentDidMount(){

        let fileList = await getFiles();

        this.setState({
            fileList : fileList
        })
    }

    render(){

        return(
            <>
                <FormHeader formHeader={'list:fileListTitle'}  VERTICAL={VERTICAL} HORIZONTAL={HORIZONTAL} layout={this.state.layout} onChangeLayout={this.onChangeLayout} onUpdateScrollTo={this.onUpdateScrollTo} onScrollTo={this.onScrollTo} onUpdateAlign={this.onUpdateAlign}/>
                    <AutoSizer>
                        {({ height, width }) => (
                        <List
                            height={height-145}
                            itemCount={3}
                            itemSize={90}
                            width={width}
                            data={this.state.isEdit}
                            itemData={this.state.fileList}
                            layout={this.state.layout}
                            useIsScrolling {...this.props}
                            ref={this.listRef}
                            className='listArea'
                        >
                            {LangRowTemplate}
                        </List>
                        )}
                </AutoSizer>
            </>
            
        )
    }
}

export default LangManager;