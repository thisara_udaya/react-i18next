import React, { Component } from 'react';
import qs from 'qs';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';

import {
    getFile,
    updateFile
}from '../../services';

class LanguageEditor extends Component {
    
    state = {
        langFile : {},
        updatedLangFile : {},
        responseMessage : 'Saving'
    }

    async componentDidMount(){

        let file = {};

        const moduleName = this.props.location.state.modulename;
        const fileName = this.props.location.state.filename;

        file = await getFile(moduleName, fileName);

        this.setState({
            langFile : (Object.keys(file).length > 0) ? file.split(",").join(",\n") : 'No File Data!',
            moduleName : moduleName,
            fileName : fileName,
            responseMessage : ''
        })
    }

    fileHandler(e){
        this.setState({
            updatedLangFile : e.target.value.split(",").join(",\n")
        })
    }

    async saveFile(){

        let saveStatusMessage;

        const data = {
            'module': this.props.location.state.modulename,
            'locale': this.props.location.state.filename,
            'filedata' : (Object.keys(this.state.updatedLangFile).length>0)? (this.state.updatedLangFile).replace(/\r?\n|\r/g, '') : this.state.langFile.replace(/\r?\n|\r/g, '')
        }

        let response = await updateFile(qs.stringify(data));
 
        if(response === 200){
            saveStatusMessage = 'File updated successfully.!';
        }else{
            saveStatusMessage = 'Error updating file.!'
        }

        this.setState({
            responseMessage : saveStatusMessage
        })
    }

    render(){

    return (
        <Container className='formHeader'>
            <Row>
                <Col>
                    <h2 className='formTitle'>Language Bundle Editor</h2>
                </Col>
            </Row>
            <Row>
                <Col>
                <Form.Group>
                    <Form.Label>{this.state.moduleName} / {this.state.fileName}</Form.Label>
                    <Form.Control as='textarea' rows='10' cols='100' defaultValue={(this.state.langFile)} onChange={this.fileHandler.bind(this)}/>
                </Form.Group>
                    <Button onClick={this.saveFile.bind(this)}>Save</Button>
                    <Form.Label className='messageStyle'>{this.state.responseMessage}</Form.Label>
                </Col>
            </Row>
        </Container>
    )}
}

export default LanguageEditor;