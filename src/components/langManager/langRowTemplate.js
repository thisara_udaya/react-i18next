import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class LangRowTemplate extends Component{

    render(){
        const { data, index, style } = this.props;

        var fileList = data["fileList"];
        let moduleName = '';
        let enFile = '';
        let esFile = '';

        for(var k in fileList){
            var f1 = fileList[index];
            moduleName = (f1["file"]["dirName"]);
            enFile = (f1["file"]["files"][0]);
            esFile = (f1["file"]["files"][1]);
            break;
        } 

        return (
        <Container>
            <Row className={index % 2 ? 'ListItemOdd' : 'ListItemEven'} style={style}>
                <Col className='numberCellStyle'>{(index+1)}</Col>
                <Col>{moduleName}</Col>
                <Col><Link to={{
                                pathname:'/lang-editor',
                                state:{filename:enFile, modulename:moduleName}
                                }}>English</Link></Col>
                <Col><Link to={{
                                pathname:'/lang-editor',
                                state:{filename:esFile, modulename:moduleName}
                                }}>Spanish</Link></Col>
                <Col></Col>
                <Col></Col>
                <Col></Col>
                <Col></Col>
                <Col></Col>
                <Col></Col>
            </Row>
        </Container> )
    }
}

export default LangRowTemplate;