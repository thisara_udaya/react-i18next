import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import AutoSizerList from './components/listAutoSized';
import HomePage from './components/home';
import LanguageButton from './components/commons/languageButton';
import Localizer from './components/commons/intl/localizer';
import LangManager from './components/langManager';
import LanguageEditor from './components/langManager/languageEditor';

function App() {
  return (
    <Router>
    <Container fluid>
      <Row className='headStyle'>
        <Col>
          <Localizer message='commons:header.mainHeader'/>
        </Col>
        <Col>
          <LanguageButton/>
        </Col>
      </Row>
      <Row className='contentArea'>
        <Col lg={2} className='navLinks'>
          <div className="p-1 mb-2 bg-light text-white">
            <Link className="text-info" to="/home"><Localizer message='commons:navigation.home'/></Link>
          </div>
          <div className="p-1 mb-2 bg-light text-white">
            <Link className="text-info" to="/auto-size-list"><Localizer message='commons:navigation.dataList'/></Link>
          </div>
          <div className="p-1 mb-2 bg-light text-white">
          <Link className="text-info" to="/lang-manager"><Localizer message='commons:navigation.langManager'/></Link>
          </div>
        </Col>
        <Col lg={10}>
          <Switch>
            <Route path ='/auto-size-list' component={AutoSizerList}/>
            <Route path = '/home' component={HomePage}/>
            <Route path = '/lang-manager' component={LangManager}/>
            <Route path = '/lang-editor' component={LanguageEditor}/>
          </Switch>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className='footerStyle'><Localizer message='commons:footer.copyrightNote'/></div>
        </Col>
      </Row>
    </Container>
    </Router>
  );
}

export default App;
