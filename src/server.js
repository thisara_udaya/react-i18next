const express = require('express');
const path = require('path');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/files', (req, res) => {
  
  const dirPath = './assets/lang';

  res.setHeader('Access-Control-Allow-Origin','*');
  res.json(getFileList(dirPath));
})

app.get('/file/:module/:locale', (req, res) => {

  let moduleName = req.params.module;
  let locale = req.params.locale;
  const filePath = './assets/lang/'+moduleName+'/'+locale; 

  file = fs.readFileSync(filePath, {encoding:'utf8', flag:'r'}); 

  res.setHeader('Access-Control-Allow-Origin','*');
  res.json(file);
})

app.post('/file',(req, res) => {

  let moduleName = req.body.module;
  let locale = req.body.locale;
  const filePath = './assets/lang/'+moduleName+'/'+locale; 

  let data = JSON.stringify(req.body.filedata);
  var obj = JSON.parse(data.replace(/\r?\n|\r/g, ''));

  fs.writeFileSync(filePath, obj);
  res.send('file operation completed.');
})

function getFileList(dir) {

    let tree = {
      fileList : []
    }

    foldernames = fs.readdirSync(dir); 

    foldernames.forEach(dfile => { 
      var name = path.join(dir, dfile);
      var isDir = fs.statSync(name).isDirectory();
      var files = [];
 
      if(isDir){

        fileNames = fs.readdirSync(name);

        fileNames.forEach(file => {
          files.push(file);     
        })

        tree.fileList.push({
          file : {
                  dirName : dfile,
                  files : files
          }      
        });

      }else{
        console.log('invalid file location');
      }
    }); 
    return tree;
  }

const port = process.env.PORT || 8080;

app.listen(port);

console.log('App is listening on port ' + port);