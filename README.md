##About this project
This is a example application created with create-react-app to demonstrate application of react-i18n (https://github.com/i18next/react-i18next).

#How to setup

##pre-requisites

- npm
- node > 10
- react-version > 16.8

##Project setup

1. Clone the project
2. npm install

#How to run

##Step 1 - Start the Node Server

1. cd src/
2. node server.js
3. http://localhost:8080/files
   - This should return list of language bundles

##Step 2 - Start UI Server

1. npm start
2. http://localhost:3000

